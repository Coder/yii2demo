<?php
/**
 * 用户初始化
 * @author: Jason<18651617068@139.com> (2016年07月27日 17:11)
 */

namespace console\controllers;

use backend\models\MigAdmin;
class InitController extends \yii\console\Controller
{
    /**
     * 创建初始化用户
     * @author Jason<18651617068@139.com>
     */
    public function actionUser()
    {
        echo "Create Init User ...\n";
        $username = $this->prompt('Input UserName : ');
        $email = $this->prompt("Input Email for $username : ");
        $password = $this->prompt("Input Password for $username : ");

        $model = new MigAdmin();
        $model->username = $username;
        $model->email = $email;
        $model->password = $password;

        if(!$model->save()){
            foreach($model->getErrors() as $errors){
                foreach($errors as $e){
                    echo "$e\n";
                }
            }
            return 1;
        }
        return 0;
    }

}