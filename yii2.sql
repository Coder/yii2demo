/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yii2

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2017-03-07 19:14:25
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migadmin
-- ----------------------------
DROP TABLE IF EXISTS `migadmin`;
CREATE TABLE `migadmin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migadmin
-- ----------------------------
INSERT INTO `migadmin` VALUES ('1', 'admin', '', '$2y$13$Q9D695vZd4Fb4sWNaJmd6.rsDowvfUrQzoyDgvTL371og/hxgOa16', null, 'admin@admin.com', '10', '10', '1469613736', '1469613736');

-- ----------------------------
-- Table structure for migration
-- ----------------------------
DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of migration
-- ----------------------------
INSERT INTO `migration` VALUES ('m130524_201442_init', '1469609541');
INSERT INTO `migration` VALUES ('m160727_085326_test', '1469674952');
INSERT INTO `migration` VALUES ('m160728_014443_test2', '1469675193');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` smallint(6) NOT NULL DEFAULT '10',
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'tutu', 'klUp-OJYxPSi-o3csMwgdXqG7SXNWYaw', '$2y$13$V/RqLBw8tcniubauBafb9.p77NBFOqtL5cOOtG8ir6mFeThcKdwta', null, 'tutu@163.com', '10', '10', '1488883421', '1488883421');
INSERT INTO `user` VALUES ('2', 'tutu2', 'I3IwOy_g7FkKHFf6CoDLYBJ9Miabl2dY', '$2y$13$sodKbqfLmi3UT43E60I00.4uH.hnYKQd2SOu9Dbo2Z4VXNHTFuUz6', null, 'tutu2@qq.com', '10', '10', '1488883930', '1488883930');
INSERT INTO `user` VALUES ('3', 'jason', 'F5DoBAE5KI_P9SZZimIuzRZ0WesCPsA6', '$2y$13$ViJbQrC7fenXp/vtrV8pGO8MDA3v3XGvEapyfmTfWlqzFIzP6.wTq', 'MEjFbafXhc_I5U86WVXr4M031LIhZndS_1488885036', '541899523@qq.com', '10', '10', '1488885024', '1488885036');
