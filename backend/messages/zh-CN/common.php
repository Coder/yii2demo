<?php
/**
 * User: MYL
 * Date: 2016/6/23 17:04
 */
return [
    'input_form' => '请填写下面的表单登录:',
    'login' => '登录',
    'index' => '首页',
    'admin_index' => '后台首页',
    'admin_index_title' => '登录 - 后台管理',
    'username_or_passwd_error' => '用户名或密码错误',
];